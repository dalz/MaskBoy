.POSIX:
.PHONY: maskboy flash distclean

maskboy: build/Makefile
	$(MAKE) -C build

flash: maskboy
	picotool load -f build/maskboy.uf2

build/Makefile:
	cmake -B build

distclean:
	-rm -r build

