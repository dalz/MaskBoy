#include "pico/stdlib.h"
#include "hardware/spi.h"

#define SPI spi0
#define BAUDRATE (75*1000*1000) /* TODO */

#define MISO 0
#define CS 1
#define SCK 2
#define MOSI 3
#define RESET 5
#define DC_RS 4

enum ili_cmd {
	SLEEP_OUT = 0x11,
	DISPLAY_ON = 0x29,
	COLUMN_ADDRESS_SET = 0x2A,
	PAGE_ADDRESS_SET = 0x2B,
	MEMORY_WRITE = 0x2C,
	MEMORY_ACCESS_CONTROL = 0x36,
	PIXEL_FORMAT_SET = 0x3A,
	POWER_CONTROL_2 = 0xC1,
	POWER_CONTROL_B = 0xCF,
	POWER_ON_SEQUENCE_CONTROL = 0xED,
	PUMP_RATIO_CONTROL = 0xF7,
};

static uint8_t init_sequence[] = {
	POWER_CONTROL_2, 1, 0x03,
	POWER_CONTROL_B, 3, 0x00, 0xE1, 0x30, /* TODO PCEQ, DRV_ena? */
	POWER_ON_SEQUENCE_CONTROL, 4, 0x64, 0x01, 0x12, 0x81, /* TODO not a clue */
	PUMP_RATIO_CONTROL, 1, 0x20,

	MEMORY_ACCESS_CONTROL, 1, 0xC8,              /* 180° rotation, RGB */
	PIXEL_FORMAT_SET, 1, 0x55,                   /* 16 bits */

	/* TODO DRIVER_TIMING_CONTROL_A? */
	/* TODO VCOM 1, VCOM 2*/
	/* TODO frame rate control */
	/* TODO display function control */
	/* TODO enable 3 gamma, gamma correction */

	DISPLAY_ON, 0,
	SLEEP_OUT, 0
};

/* TODO needed? can connect to GND? */
static inline void ili_select(void) { gpio_put(CS, 0); }
static inline void ili_deselect(void) { /* gpio_put(CS, 1); */ }

void
ili_send_cmd(enum ili_cmd cmd)
{
	gpio_put(DC_RS, 0);
	spi_write_blocking(SPI, &cmd, 1);
	gpio_put(DC_RS, 1);
}

/* assumes that DC_RS is low */
static inline void
ili_send_data(size_t len, uint8_t data[len])
{
	/* TODO maybe inline */
	spi_write_blocking(SPI, data, len);
}

#include "prencipe.c"
void
ili_init(void)
{
	spi_init(SPI, BAUDRATE); /* TODO check actual value set */

	/* TODO buffer/transfer size */

	gpio_set_function(MISO, GPIO_FUNC_SPI);
	gpio_set_function(SCK, GPIO_FUNC_SPI);
	gpio_set_function(MOSI, GPIO_FUNC_SPI);

	gpio_init(CS);
	gpio_init(RESET);
	gpio_init(DC_RS);

	gpio_set_dir(CS, GPIO_OUT);
	gpio_set_dir(RESET, GPIO_OUT);
	gpio_set_dir(DC_RS, GPIO_OUT);

	gpio_put(CS, 1); /* p. 33 */
	gpio_put(RESET, 1); /* active-low */

	sleep_ms(50); /* TODO */
	ili_select();

	int i = 0;
	while (i < count_of(init_sequence)) {
		ili_send_cmd(init_sequence[i]);
		ili_send_data(init_sequence[i + 1], &init_sequence[i + 2]);
		i += 2 + init_sequence[i + 1];
	}

	/* needed after SLEEP_OUT */
	sleep_ms(5);

	ili_send_cmd(MEMORY_WRITE);
	/* for (uint16_t i = 0; ; i += 127) */
	/* for (int j = 0; j < 240 * 320; j++) */
	/* 	ili_send_data(2, (char*)&i); */

	/* uint16_t c = 0xF248; */
	/* for (int j = 0; j < 240 * 320; j++) */
	/* 	ili_send_data(2, &c); */


	unsigned n = 320 * 240 * 2;
	uint8_t *img = gimp_image.pixel_data;
	/* uint8_t img[320*240*2]; */
	for (unsigned i = 0; i < n; i += 2)  {
		/* img[i] = 0xF8; */
		/* img[i + 1] = 0x00; */
		/* img[i] = (img[i] << 8) | (img[i] >> 8); */
		uint8_t tmp = img[i];
		img[i] = img[i + 1];
		img[i + 1] = tmp;
	}

	uint16_t *img16 = img;
	for (unsigned i = 0; i < 320; i++)
	for (unsigned j = i + 1; j < 240; j++) {
		uint16_t tmp = img16[i * 320 + j];
		img16[i * 320 + j] = img16[j * 320 + i];
		img16[j * 320 + i] = tmp;
	}

	/* for (;;){ */
	/* ili_send_data(1, img); */
	/* sleep_ms(1);} */

	ili_send_data(10000, img);
	for (;;);

	ili_deselect();
}

/*
ILI9341_SOFTRESET,0,                 //Soft Reset
TFTLCD_DELAY8, 50, 
ILI9341_DISPLAYOFF, 0,            //Display Off
ILI9341_INTERFACECONTROL, 3, 0x01, 0x01, 0x00,  //Interface Control needs EXTC=1 MV_EOR=0, TM=0, RIM=0
ILI9341_RGBSIGNAL, 1, 0x00,      //RGB Signal [00] 
ILI9341_INVERSIONCONRTOL, 1, 0x00,      //Inversion Control [02] .kbv NLA=1, NLB=1, NLC=1
ILI9341_SLEEPOUT, 0,            //Sleep Out
ILI9341_DISPLAYON, 0          //Display On
*/
